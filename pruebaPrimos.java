package numeroprimo;

import java.util.Scanner;
/**
 *
 * @author alh19
 */
public class NumeroPrimo {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        int num;
        
        System.out.println("Determinación de número primos");
        System.out.print("Introduce un número natural: ");
        num = entrada.nextInt();
        
        if(num%2==0){
            System.out.println("El número no es primo");
        } else if(num%3==0){
            System.out.println("El número no es primo");
        } else if(num%5==0){
            System.out.println("El número no es primo");
        } else if(num%7==0){
            System.out.println("El número no es primo");
        } else {
            System.out.println("El número es primo");
        }
    }
    
}